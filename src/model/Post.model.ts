import { User } from "./User.model";
import { Category } from "./Category.model";

export interface Post {
  id?: number;
  content: string;
  status?: string;
  title: boolean;
  mainImage?: string;
  createdAt?: string;
  updatedAt?: string;
  colaborators?: User[];
  categories?: Category[];
}
