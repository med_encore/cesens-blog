export interface User {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  administrator?: boolean;
  photo?: string;
  description?: string;
  gender?: string;
  password?: string;
  linkedin?: string;
  facebook?: string;
  createdAt?: string;
  updatedAt?: string;
}

export interface UserAuth {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  administrator?: string;
  photo: string;
  exp: number;
  token: string;
}
