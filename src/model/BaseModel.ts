export abstract class BaseModel {
  public objectType: string = null;

  public static normalize(str: string): string {
    return str
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/, "")
      .toLowerCase();
  }

  protected static datesEqual(date1: Date, date2: Date): boolean {
    if (date1 !== null && date2 !== null) {
      return date1.getTime() === date2.getTime();
    } else {
      return date1 === date2;
    }
  }

  protected static arraysEqual<T>(array1: T[], array2: T[]): boolean {
    if (array1 === array2) {
      return true;
    }
    if (array1.length !== array2.length) {
      return false;
    }
    const copia = array2.slice();
    for (const valor of array1) {
      const indexOn2 = copia.indexOf(valor);
      if (indexOn2 === -1) {
        return false;
      }
      copia.splice(indexOn2, 1);
    }
    return copia.length === 0;
  }

  public toString(): string {
    return JSON.stringify(this);
  }

  public populate(params?: object): this {
    if (!params) {
      return this;
    }
    for (const key of Object.keys(params)) {
      const propDescriptor = Object.getOwnPropertyDescriptor(this, key);
      if (propDescriptor !== undefined && propDescriptor.writable) {
        try {
          // @ts-ignore
          this[key] = params[key];
        } catch (e) {
          if (!(e instanceof TypeError)) {
            throw e;
          }
        }
      }
    }
    return this;
  }

  public toJSON(): ObjectInterface {
    const json = { ...(this as ObjectInterface) };
    delete json.objectType;
    return json;
  }
}

export abstract class BaseModelId extends BaseModel {
  public id: number = null;

  public static arrayToObject<T extends BaseModelId>(
    list: T[]
  ): ObjectInterface<T> {
    const object: ObjectInterface<T> = {};
    for (const item of list) {
      object[item.id] = item;
    }
    return object;
  }
}

export abstract class BaseModelIdNombre extends BaseModelId {
  public nombre: string = null;
  private normalizedNombre: string = null;

  public findByNombre(filtro: string): boolean {
    return this.getNormalizedNombre().includes(filtro);
  }

  protected getNormalizedNombre(): string {
    if (this.normalizedNombre === null) {
      this.normalizedNombre = BaseModel.normalize(this.nombre);
    }
    return this.normalizedNombre;
  }

  public toJSON(): ObjectInterface {
    const json = super.toJSON();
    delete json.normalizedNombre;
    return json;
  }
}

export interface ObjectInterface<T = any, Id = "id"> {
  [Id: string]: T;
}
