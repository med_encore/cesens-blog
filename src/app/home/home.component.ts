import { Component, OnInit, OnDestroy } from "@angular/core";
import { NavService } from "src/services/components/nav.service";
import { Post } from "src/model/Post.model";
import { Subscription } from "rxjs";
import { CategoryService } from "src/services/api/category.service";
import { PostService } from "src/services/api/post.service";
import { Category } from "src/model/Category.model";
import { PageChangedEvent } from "ngx-bootstrap/pagination/public_api";
import { BaseService } from "src/services/api/base.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
  // loading
  loading = true;

  // fetched posts
  posts: Post[];
  postsSub: Subscription;
  length: number;

  // last2Posts
  lastPosts: Post[];
  lastPostsSub: Subscription;

  // categories
  categories: Category[];
  categoriesSub: Subscription;

  constructor(
    private nav: NavService,
    private catgoryService: CategoryService,
    private postsService: PostService,
    private baseService: BaseService
  ) {}

  ngOnInit() {
    // test loading
    /*    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000); */
    // show nav bars
    this.nav.show();

    // fetch posts
    this.fetchPosts(null, 1, 8);
    // fetch all categries
    this.categoriesSub = this.catgoryService
      .fetchAllCategories(null, null)
      .subscribe(
        data => {
          this.categories = data;
        },
        err => console.log(err),
        () => console.log("fetched categories notification")
      );

    // fetch last 2 posts
    this.lastPostsSub = this.postsService.fetchAllPosts(1, 2).subscribe(
      data => {
        this.lastPosts = data.posts;
      },
      err => console.log(err),
      () => {
        console.log("fetched posts notification");
        this.loading = false;
      }
    );
  }

  onSelectCategory(id: number) {
    // todo idk
  }

  fetchPosts(id: number, page: number, limit: number) {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
    if (id !== null) {
      this.postsSub = this.catgoryService
        .fetchAllPostsByCategory(Number(id), page, limit)
        .subscribe(
          data => {
            this.posts = data.posts;
            this.length = data.length;
          },
          err => console.log(err),
          () => {
            console.log("fetched posts by category notification");
            const regex = /(<([^>]+)>)/gi;
            this.posts.forEach(p => {
              const txt = p.content.replace(regex, "");
              p.content = txt;
            });
          }
        );
    } else {
      this.postsSub = this.postsService.fetchAllPosts(page, limit).subscribe(
        data => {
          this.posts = data.posts;
          this.length = data.length;
        },
        err => console.log(err),
        () => {
          console.log("fetched posts notification");
          this.posts.forEach(p => {
            p.content = this.baseService.stripHtml(p.content.substr(0, 90));
          });
        }
      );
    }
  }

  pageChanged(event: PageChangedEvent): void {
    console.log(event.page);
    this.fetchPosts(null, event.page, event.itemsPerPage);
  }

  ngOnDestroy() {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
    if (this.lastPostsSub) {
      this.lastPostsSub.unsubscribe();
    }
    if (this.categoriesSub) {
      this.categoriesSub.unsubscribe();
    }
  }
}
