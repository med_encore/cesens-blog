import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./auth/login/login.component";
import { PageNotFoundComponent } from "./other/page-not-found/page-not-found.component";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  {
    path: "category",
    loadChildren: "./category/category.module#CategoryModule"
  },
  { path: "post", loadChildren: "./posts/post.module#PostModule" },
  { path: "user", loadChildren: "./user/user.module#UserModule" },
  { path: "login", component: LoginComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
