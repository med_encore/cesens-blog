import { Component, OnInit, TemplateRef } from "@angular/core";
import { NavService } from "src/services/components/nav.service";
import { AuthService } from "src/app/auth/auth.service";
import { NgForm } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Router } from "@angular/router";
import { AlertService } from "ngx-alerts";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  constructor(
    private nav: NavService,
    private authService: AuthService,
    private modalService: BsModalService,
    private router: Router,
    private alertService: AlertService
  ) {}

  // private model
  pwdModalRef: BsModalRef;

  isAuth: boolean;

  msg: string;

  ngOnInit() {
    this.nav.hide();

    this.isAuth = this.authService.getIsAuth();
    if (this.isAuth) {
      this.router.navigate(["/home"]);
    }
  }

  openPwdModal(pwdTemplate: TemplateRef<any>) {
    this.pwdModalRef = this.modalService.show(pwdTemplate);
  }

  onLogin(form: NgForm) {
    if (form.invalid) {
      console.log("from invalid");
      this.alertService.danger("invalid fields");
      return;
    }
    this.authService.login(form.value.email, form.value.password);
  }

  onResetPwd(form: NgForm) {
    if (form.invalid) {
      this.alertService.danger("invalid fields");
      console.log("from invalid");
      return;
    }
    this.authService.resetPassword(form.value.email).subscribe(data => {
      this.msg = data.msg;
      this.alertService.success(data.msg);
    });
  }
}
