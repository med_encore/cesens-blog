import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subject, BehaviorSubject, throwError } from "rxjs";
import { environment } from "../../environments/environment";
import { User, UserAuth } from "src/model/User.model";

const URL = environment.baseUrl + "users";

export interface AuthData {
  email: string;
  password: string;
}

@Injectable({ providedIn: "root" })
export class AuthService {
  private isAuthenticated = false;
  private token = new BehaviorSubject<string>(null);
  private tokenTimer: any;
  private userAuth: UserAuth;
  private userAuthObs = new BehaviorSubject<UserAuth>(null);
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token.asObservable();
  }

  getUser() {
    return this.userAuthObs.asObservable();
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  resetPassword(email: string) {
    return this.http.post<{ msg: string }>(`${URL}/reset-pwd`, { email });
  }

  createUser(data: User) {
    this.http
      .post<{ user: User }>(`${URL}`, { ...data })
      .subscribe(
        response => {
          this.router.navigate(["/auth/login"]);
        },
        error => {
          this.authStatusListener.next(false);
        }
      );
  }

  login(email: string, password: string) {
    const authData: AuthData = { email, password };
    this.http.post<UserAuth>(URL + "/login", authData).subscribe(
      response => {
        this.token.next(response.token);
        console.log(response);

        if (response.token) {
          const expirationDate = response.exp;
          const now = new Date();
          const expiresInDuration = expirationDate - now.getTime() / 1000;
          this.setAuthTimer(expiresInDuration);
          this.isAuthenticated = true;
          this.userAuth = response;
          this.userAuthObs.next(this.userAuth);
          this.authStatusListener.next(true);
          this.saveAuthData(this.userAuth);
          this.router.navigate(["/"]);
        }
      },
      error => {
        this.handleError(error);
      }
    );
  }

  autoAuthUser() {
    const auth = this.getAuthData();
    if (!auth) {
      return;
    }
    const now = new Date();
    const expiresIn = auth.exp - now.getTime() / 1000;
    if (expiresIn > 0) {
      this.isAuthenticated = true;
      this.userAuth = auth;
      this.userAuthObs.next(this.userAuth);
      this.setAuthTimer(expiresIn);
      this.authStatusListener.next(true);
      this.token.next(auth.token);
    }
  }

  logout() {
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.userAuth = null;
    this.userAuthObs.next(null);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(["/login"]);
  }

  private setAuthTimer(duration: number) {
    // duration in seconds
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(user: UserAuth) {
    localStorage.setItem("auth", JSON.stringify(user));
  }

  private clearAuthData() {
    localStorage.removeItem("auth");
  }

  private getAuthData() {
    const auth: UserAuth = JSON.parse(localStorage.getItem("auth"));
    if (!auth) {
      return;
    }
    return auth;
  }

  handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Message: ${error.error.msg}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
