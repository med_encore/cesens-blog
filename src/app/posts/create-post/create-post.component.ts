import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { CategoryService } from "src/services/api/category.service";
import { Category } from "src/model/Category.model";
import { UserService } from "src/services/api/user.service";
import { User, UserAuth } from "src/model/User.model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PostService } from "src/services/api/post.service";
import { AuthService } from "src/app/auth/auth.service";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Post } from "src/model/Post.model";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import Quill from "quill";
import { AlertService } from "ngx-alerts";

// add image resize module
import ImageResize from "quill-image-resize-module";
Quill.register("modules/imageResize", ImageResize);

// override p with div tag
const Parchment = Quill.import("parchment");
const Block = Parchment.query("block");

Block.tagName = "DIV";
// or class NewBlock extends Block {}; NewBlock.tagName = 'DIV';
Quill.register(Block /* or NewBlock */, true);

import Counter from "./counter";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
Quill.register("modules/counter", Counter);

// Add fonts to whitelist
const Font = Quill.import("formats/font");
// We do not add Aref Ruqaa since it is the default
Font.whitelist = ["mirza", "aref", "sans-serif", "monospace", "serif"];
Quill.register(Font, true);

@Component({
  selector: "app-create-post",
  templateUrl: "./create-post.component.html",
  styleUrls: ["./create-post.component.css"]
})
export class CreatePostComponent implements OnInit, OnDestroy {
  // MODE EDIT OR CREATE POST
  private id: string;
  mode: string;

  // The form
  postForm: FormGroup;
  get title() {
    return this.postForm.get("title");
  }
  get categories() {
    return this.postForm.get("categories");
  }
  get collaborators() {
    return this.postForm.get("collaborators");
  }
  get mainImage() {
    return this.postForm.get("mainImage");
  }
  get content() {
    return this.postForm.get("content");
  }

  // current user
  user: UserAuth;
  userSub: Subscription;

  // image inupt
  optionsImg = {
    fileSize: 2048, // in Bytes (by default 2048 Bytes = 2 MB)
    minWidth: 0, // minimum width of image that can be uploaded (by default 0, signifies any width)
    maxWidth: 0, // maximum width of image that can be uploaded (by default 0, signifies any width)
    minHeight: 0, // minimum height of image that can be uploaded (by default 0, signifies any height)
    maxHeight: 0, // maximum height of image that can be uploaded (by default 0, signifies any height)
    fileType: ["image/gif", "image/jpeg", "image/png"], // mime type of files accepted
    height: 411, // height of cropper
    quality: 1, // quaity of image after compression
    crop: [
      // array of objects for mulitple image crop instances (by default null, signifies no cropping)
      {
        ratio: 3.1, // ratio in which image needed to be cropped (by default null, signifies ratio to be free of any restrictions)
        minWidth: 1200, // minimum width of image to be exported (by default 0, signifies any width)
        maxWidth: 2400, // maximum width of image to be exported (by default 0, signifies any width)
        minHeight: 411, // minimum height of image to be exported (by default 0, signifies any height)
        maxHeight: 811, // maximum height of image to be exported (by default 0, signifies any height)
        width: 1200, // width of image to be exported (by default 0, signifies any width)
        height: 411 // height of image to be exported (by default 0, signifies any height)
      }
    ]
  };

  // image edit model
  ImageModalRef: BsModalRef;
  srcImg: string;

  // text editor input
  configGuill = {
    toolbar: [
      [{ header: [1, 2, 3, false] }], // custom dropdown
      ["bold", "italic", "underline", "blockquote"], // toggled buttons
      [{ list: "ordered" }, { list: "bullet" }, { align: [] }],
      [
        {
          color: [
            "#48B745",
            "#28A745",
            "#007BFE",
            "#6C757D",
            "#f8f9fq",
            "#cecece",
            "#818181",
            "#ffffff",
            "#000000"
          ]
        },
        {
          background: [
            "#48B745",
            "#28A745",
            "#007BFE",
            "#6C757D",
            "#f8f9fq",
            "#cecece",
            "#818181",
            "#ffffff",
            "#000000"
          ]
        }
      ], // dropdown with defaults from theme
      ["link", "image"], // link and image, video
      ["clean"] // remove formatting button
    ],
    imageResize: {}
  };
  quillLength = 0;

  // multi select ipnut Catgories
  dropdownListCatgories: Category[];
  selectedItemsCatgories = [];
  dropdownSettingsCatgories: IDropdownSettings = {};

  // multi select ipnut collaborators
  dropdownListcollaborators: User[];
  selectedItemscollaborators = [];
  dropdownSettingscollaborators: IDropdownSettings = {};

  // subs
  routeSub: Subscription;
  postSub: Subscription;

  // fetched post
  post: Post;
  editContent: SafeHtml;

  constructor(
    private categotyService: CategoryService,
    private userService: UserService,
    private postService: PostService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private _sanitizer: DomSanitizer,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    /*     // test loading
    this.navService.isLoading(true);
    setTimeout(() => {
      this.navService.isLoading(false);
    }, 3000); */

    // fetch categories
    this.categotyService.fetchAllCategories(null, null).subscribe(data => {
      this.dropdownListCatgories = data;
    });
    this.dropdownSettingsCatgories = {
      singleSelection: false,
      idField: "id",
      textField: "name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 4,
      allowSearchFilter: true
    };

    // get the current user (CONNECTED)
    this.userSub = this.authService.getUser().subscribe(auth => {
      this.user = auth;
    });

    // fetch collaborators (not the connected user)
    this.userService.fetchAllUsers(null, null).subscribe(
      data => {
        this.dropdownListcollaborators = data;
      },
      err => console.log(err),
      () => {
        this.dropdownListcollaborators = this.dropdownListcollaborators.filter(
          u => u.id !== this.user.id
        );
      }
    );

    this.dropdownSettingscollaborators = {
      singleSelection: false,
      idField: "id",
      textField: "firstName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 4,
      allowSearchFilter: true
    };

    // set the form
    this.postForm = this.formBuilder.group({
      title: ["", Validators.required],
      content: ["", Validators.required],
      mainImage: [null],
      categories: [this.selectedItemsCatgories, Validators.required],
      collaborators: [this.selectedItemscollaborators]
    });

    // SELECT MODE
    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("id")) {
        // todo acces by owner or admin only
        this.mode = "edit";
        // todo check if value is number
        this.id = paramMap.get("id");
        this.postSub = this.postService.fetchPost(Number(this.id)).subscribe(
          data => {
            this.post = data.post;
            this.editContent = this._sanitizer.bypassSecurityTrustHtml(
              this.post.content
            );
            console.log("content:  ", this.editContent);

            const collaborators = data.collaborators.filter(
              u => u.id !== this.user.id
            );
            this.postForm.setValue({
              title: this.post.title,
              categories: data.categories,
              collaborators,
              content: this.post.content,
              mainImage: null
            });
          },
          err => this.router.navigate(["/err"])
        );
      } else {
        this.mode = "create";
        this.id = null;
      }
    });
  }

  onSelectImage(event: string | object) {
    switch (typeof event) {
      case "string":
        this.postForm.patchValue({ mainImage: [event] });
        this.srcImg = event;
        this.postForm.get("mainImage").updateValueAndValidity();
        break;
      case "object":
        console.log(event);
        this.postForm.patchValue({ mainImage: event });
        this.postForm.get("mainImage").updateValueAndValidity();
        break;
      default:
    }
    this.postForm.patchValue({ mainImage: event });
    this.postForm.get("mainImage").updateValueAndValidity();
    this.alertService.info("photo selected");
  }

  onResetImage() {
    this.srcImg = null;
    this.alertService.info("photo removed");
  }

  openModalImage(image: TemplateRef<any>) {
    this.ImageModalRef = this.modalService.show(
      image,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  maxLengthEditor(e) {
    this.quillLength = e.editor.getLength() - 1;
    if (e.editor.getLength() > 50000) {
      e.editor.deleteText(50000, e.editor.getLength());
    }
  }

  onSubmit() {
    if (this.mode === "edit") {
      // edit mode
      const data = {
        title: this.postForm.get("title").value,
        content: this.postForm.get("content").value,
        categories: this.postForm.get("categories").value.map(u => u.id),
        collaborators: [
          ...this.postForm.get("collaborators").value.map(c => c.id),
          this.user.id
        ],
        mainImage: this.postForm.get("mainImage").value
      };
      console.log(data);

      this.postSub = this.postService.editPost(Number(this.id), data).subscribe(
        response => {
          console.log(response);
          this.router.navigate(["/post/", this.id]);
        },
        err => console.log(err),
        () => console.log("ok")
      );
    } else if (this.mode === "create") {
      // create mode
      const data = {
        title: this.postForm.get("title").value,
        content: this.postForm.get("content").value,
        categories: this.postForm.get("categories").value.map(u => u.id),
        collaborators: this.postForm.get("collaborators").value.map(c => c.id),
        mainImage: this.postForm.get("mainImage").value
      };
      console.log(data);
      this.postService.createPost(data).subscribe(
        response => {
          // todo redirect to the post page
          console.log(response);
          this.router.navigate(["/post/", response.post.id]);
        },
        err => console.log(err),
        () => console.log("post created notification")
      );
    } else {
      return this.router.navigate(["/login"]);
    }
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }
}
