import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { PostService } from "src/services/api/post.service";
import { UserAuth, User } from "src/model/User.model";
import { Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { Post } from "src/model/Post.model";
import { Category } from "src/model/Category.model";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class PostComponent implements OnInit, OnDestroy {
  // loading
  loading = true;

  // current user
  isAuth: boolean;
  user: UserAuth;
  userSub: Subscription;

  // fetchedPost
  post: Post;
  postSub: Subscription;
  idPost: string;
  content: SafeHtml;

  // fetch categories and collaborators of the post
  collaborators: User[];
  categories: Category[];

  // subscriptions
  routeSub: Subscription;
  postDeleteSub: Subscription;

  constructor(
    private router: Router,
    private postService: PostService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    // get current id
    this.routeSub = this.route.params.subscribe(
      params => (this.idPost = params.id)
    );

    // fetch the post
    this.postSub = this.postService.fetchPost(Number(this.idPost)).subscribe(
      data => {
        this.post = data.post;
        this.content = this._sanitizer.bypassSecurityTrustHtml(
          this.post.content
        );
        this.collaborators = data.collaborators;
        console.log(this.collaborators);
        this.categories = data.categories;
        console.log(this.categories);
      },
      err => console.log(err),
      () => {
        console.log("fetched posty notification");
        this.loading = false;
      }
    );

    // get user connection status
    this.isAuth = this.authService.getIsAuth();
  }

  ondelete() {
    this.postDeleteSub = this.postService
      .deletePost(Number(this.idPost))
      .subscribe(
        isDeleted => {
          isDeleted
            ? this.router.navigate(["/home"])
            : this.router.navigate(["/"]);
        },
        err => console.error("error delete articale: " + err),
        () => console.log("notification: articale deleted")
      );
  }

  ngOnDestroy() {
    if (this.postSub) {
      this.postSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.postDeleteSub) {
      this.postDeleteSub.unsubscribe();
    }
  }
}
