import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PostRoutingModule } from "./post-routing.module";

import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { QuillModule } from "ngx-quill";

import { NgxImgModule } from "ngx-img";

import { PostComponent } from "./post/post.component";
import { CreatePostComponent } from "./create-post/create-post.component";

@NgModule({
  declarations: [PostComponent, CreatePostComponent],
  imports: [
    CommonModule,
    PostRoutingModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    QuillModule.forRoot(),
    NgxImgModule
  ]
})
export class PostModule {}
