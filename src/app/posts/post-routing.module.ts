import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CreatePostComponent } from "./create-post/create-post.component";
import { PostComponent } from "./post/post.component";
import { AuthGuard } from "src/services/auth/auth-guard.service";

const routes: Routes = [
  { path: "create", component: CreatePostComponent, canActivate: [AuthGuard] },
  {
    path: "edit/:id",
    component: CreatePostComponent,
    canActivate: [AuthGuard]
  },
  { path: ":id", component: PostComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class PostRoutingModule {}
