import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";
import { NavService } from "src/services/components/nav.service";
import { Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"]
})
export class FooterComponent implements OnInit, OnDestroy {
  showCollaborators = false;
  nav: boolean;
  navSub: Subscription;

  // private model
  PrivateModalRef: BsModalRef;
  LegalModalRef: BsModalRef;
  constructor(private ns: NavService, private modalService: BsModalService) {}

  ngOnInit() {
    this.nav = this.ns.visible;
    this.navSub = this.ns.getVisible().subscribe(nav => {
      this.nav = nav;
    });
  }

  openModalLegal(privateContent: TemplateRef<any>) {
    this.PrivateModalRef = this.modalService.show(
      privateContent,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  openModalPrivacy(legalContent: TemplateRef<any>) {
    this.LegalModalRef = this.modalService.show(
      legalContent,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  ngOnDestroy() {
    this.navSub.unsubscribe();
  }
}
