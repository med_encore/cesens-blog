import { Component, OnInit, OnDestroy } from "@angular/core";
import { NavService } from "src/services/components/nav.service";
import { Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { User, UserAuth } from "src/model/User.model";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(private ns: NavService, private authService: AuthService) {}

  isAuth = false;
  private authListenerSubs: Subscription;
  nav: boolean;
  navSub: Subscription;
  user: UserAuth;
  userSub: Subscription;

  onLogout() {
    this.authService.logout();
  }

  ngOnInit() {
    this.isAuth = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.isAuth = isAuthenticated;
      });
    this.userSub = this.authService.getUser().subscribe(auth => {
      this.user = auth;
    });
    this.nav = this.ns.visible;
    this.navSub = this.ns.getVisible().subscribe(nav => {
      this.nav = nav;
    });
  }

  ngOnDestroy() {
    if (this.navSub) {
      this.navSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.authListenerSubs) {
      this.authListenerSubs.unsubscribe();
    }
  }
}
