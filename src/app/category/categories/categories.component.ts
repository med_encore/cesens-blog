import { Component, OnInit, OnDestroy } from "@angular/core";
import { Category } from "src/model/Category.model";
import { Subscription } from "rxjs";
import { CategoryService } from "src/services/api/category.service";

@Component({
  selector: "app-categories",
  templateUrl: "./categories.component.html",
  styleUrls: ["./categories.component.css"]
})
export class CategoriesComponent implements OnInit, OnDestroy {
  // categories
  categories: Category[];
  categoriesSub: Subscription;

  constructor(private catgoryService: CategoryService) {}

  ngOnInit() {
    // fetch all categries
    this.categoriesSub = this.catgoryService
      .fetchAllCategories(null, null)
      .subscribe(
        data => {
          this.categories = data;
        },
        err => console.log(err),
        () => console.log("fetched categories notification")
      );
  }

  ngOnDestroy() {
    if (this.categoriesSub) {
      this.categoriesSub.unsubscribe();
    }
  }
}
