import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CreateCategoryComponent } from "./create-category/create-category.component";
import { CategoryPostsComponent } from "./category-posts/category-posts.component";
import { CategoriesComponent } from "./categories/categories.component";
import { AuthGuard } from "src/services/auth/auth-guard.service";

const routes: Routes = [
  { path: "all", component: CategoriesComponent },
  {
    path: "create",
    component: CreateCategoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "edit/:id",
    component: CreateCategoryComponent,
    canActivate: [AuthGuard]
  },
  { path: ":id/:name", component: CategoryPostsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class CategoryRoutingModule {}
