import { Component, OnInit, OnDestroy } from "@angular/core";
import { Post } from "src/model/Post.model";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { CategoryService } from "src/services/api/category.service";
import { PageChangedEvent } from "ngx-bootstrap/pagination/public_api";
import { BaseService } from "src/services/api/base.service";

@Component({
  selector: "app-category-posts",
  templateUrl: "./category-posts.component.html",
  styleUrls: ["./category-posts.component.css"]
})
export class CategoryPostsComponent implements OnInit, OnDestroy {
  // loading
  loading = true;

  // id category
  categoryId: string;
  categoryName: string;

  // fetched posts
  posts: Post[];
  postsSub: Subscription;
  length: number;

  // last2Posts
  lastPosts: Post[];

  // subscriptions
  routeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private catgoryService: CategoryService,
    private baseService: BaseService
  ) {}

  ngOnInit() {
    // get current id
    this.routeSub = this.route.params.subscribe(params => {
      this.categoryId = params.id;
      this.categoryName = params.name;
    });

    // fetchPosts
    this.fetchPosts(this.categoryId, 1, 8, true);
  }

  fetchPosts(id: string, page: number, limit: number, isFirst: boolean) {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
    this.postsSub = this.catgoryService
      .fetchAllPostsByCategory(Number(id), page, limit)
      .subscribe(
        data => {
          console.log(data);
          this.posts = data.posts;
          this.posts.forEach(p => {
            p.content = this.baseService.stripHtml(p.content.substr(0, 90));
          });

          if (isFirst) {
            this.lastPosts = this.posts.slice(0, 2);
          }
          console.log("lastPosts: ", this.lastPosts);

          this.length = data.length;
        },
        err => console.log(err),
        () => (this.loading = false)
      );
  }

  pageChanged(event: PageChangedEvent): void {
    const isFirst = event.page === 1 ? true : false;
    this.fetchPosts(this.categoryId, event.page, event.itemsPerPage, isFirst);
  }

  ngOnDestroy() {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
