import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CategoryPostsComponent } from "./category-posts/category-posts.component";
import { CategoriesComponent } from "./categories/categories.component";
import { CreateCategoryComponent } from "./create-category/create-category.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CategoryRoutingModule } from "./category-routing.module";
import { PaginationModule } from "ngx-bootstrap/pagination";

@NgModule({
  declarations: [
    CategoryPostsComponent,
    CategoriesComponent,
    CreateCategoryComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CategoryRoutingModule,
    PaginationModule
  ]
})
export class CategoryModule {}
