import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CategoryService } from "src/services/api/category.service";
import { Category } from "src/model/Category.model";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription } from "rxjs";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { AlertService } from "ngx-alerts";

@Component({
  selector: "app-create-category",
  templateUrl: "./create-category.component.html",
  styleUrls: ["./create-category.component.css"]
})
export class CreateCategoryComponent implements OnInit, OnDestroy {
  // private model
  ModalRef: BsModalRef;

  // categorie info
  category: Category;
  selectedIcon: string;
  Catform: FormGroup;
  get name() {
    return this.Catform.get("name");
  }
  get icon() {
    return this.Catform.get("icon");
  }
  private id: string;
  mode: string;

  // subscriptions
  categorySub: Subscription;
  routeSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.Catform = this.formBuilder.group({
      name: ["", Validators.required],
      icon: ["", Validators.required]
    });
    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("id")) {
        this.mode = "edit";
        this.id = paramMap.get("id");
        // check if value is number
        this.categorySub = this.categoryService
          .fetchCategory(Number(this.id))
          .subscribe(
            data => {
              this.category = data;
              this.selectedIcon = this.category.icon;
              this.Catform.setValue({
                name: this.category.name,
                icon: this.category.icon
              });
            },
            err => this.router.navigate(["/ouuuuuups"])
          );
      } else {
        this.mode = "create";
        this.id = null;
      }
    });
  }

  openModal(Content: TemplateRef<any>) {
    this.alertService.info("Select an icon for the category");
    this.ModalRef = this.modalService.show(
      Content,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  onSubmit() {
    const data = {
      name: this.Catform.get("name").value,
      icon: this.Catform.get("icon").value
    };
    if (this.mode === "edit") {
      this.categorySub = this.categoryService
        .editCategory(Number(this.id), data)
        .subscribe(() => {
          this.alertService.success("category modified successfully");
          this.router.navigate(["/user/setting/categories"]);
        });
    } else if (this.mode === "create") {
      this.categorySub = this.categoryService
        .createCategory(data)
        .subscribe(() => {
          this.alertService.success("category created successfully");
          this.router.navigate(["/user/setting/categories"]);
        });
    } else {
      return this.router.navigate(["/login"]);
    }
  }

  ondelete() {
    if (this.mode === "edit") {
      this.categorySub = this.categoryService
        .deleteCategory(Number(this.id))
        .subscribe(
          isDeleted => {
            isDeleted
              ? this.router.navigate(["/post/create"])
              : this.router.navigate(["/"]);
          },
          err => console.error("error delete category: " + err),
          () => {
            this.alertService.success("category deleted successfully");
          }
        );
    }
  }

  selectIcon(icon) {
    this.selectedIcon = icon.target.className;
    this.Catform.patchValue({ icon: this.selectedIcon });
    this.alertService.success({
      html: "<p><span class='" + this.selectedIcon + "'></span> selected </p>"
    });
    this.ModalRef.hide();
  }

  ngOnDestroy() {
    if (this.categorySub) {
      this.categorySub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
