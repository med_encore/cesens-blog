import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CreateUserComponent } from "./create-user/create-user.component";
import { SettingComponent } from "./setting/setting.component";
import { ProfileComponent } from "./profile/profile.component";
import { AuthGuard } from "src/services/auth/auth-guard.service";

const routes: Routes = [
  { path: "create", component: CreateUserComponent, canActivate: [AuthGuard] },
  {
    path: "edit/:id",
    component: CreateUserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "setting/account",
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "setting/security",
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "setting/users",
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "setting/categories",
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "setting/posts",
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  { path: "profile", component: ProfileComponent },
  { path: "profile/:id", component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class UserRoutingModule {}
