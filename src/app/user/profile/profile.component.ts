import { Component, OnInit, OnDestroy } from "@angular/core";
import { UserAuth, User } from "src/model/User.model";
import { Subscription } from "rxjs";
import { Post } from "src/model/Post.model";
import { Router, ParamMap, ActivatedRoute } from "@angular/router";
import { UserService } from "src/services/api/user.service";
import { AuthService } from "src/app/auth/auth.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class ProfileComponent implements OnInit, OnDestroy {
  // loading
  loading = true;

  // current user
  isAuth: boolean;
  user: UserAuth;
  userSub: Subscription;

  // fetched profile
  fetchedUser: User;
  postSub: Subscription;
  idUser: number;

  // fetch posts of the user
  posts: Post[];

  // all users
  users: User[];
  usersSub: Subscription;

  // subscriptions
  routeSub: Subscription;

  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loading = true;
    // get the current user (CONNECTED)
    this.userSub = this.authService.getUser().subscribe(auth => {
      this.user = auth;
    });

    // set the id of the user
    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("id")) {
        this.idUser = Number(paramMap.get("id"));
      } else {
        this.idUser = this.user.id;
      }
    });

    // fetch the user
    this.userSub = this.userService.fetchUser(this.idUser).subscribe(
      user => {
        this.fetchedUser = user;
      },
      err => console.log(err),
      () => console.log("user fetched notification")
    );

    // fetch the posts to the user
    this.postSub = this.userService
      .fetchAllPostsByUser(this.idUser, 1, 8)
      .subscribe(
        posts => {
          this.posts = posts;
        },
        err => console.log(err),
        () => console.log("fetched posts notification")
      );

    // fetch all users
    this.userService.fetchAllUsers(null, null).subscribe(
      data => {
        this.users = data;
      },
      err => console.log(err),
      () => {
        this.users = this.users.filter(u => u.id !== this.idUser);
        this.loading = false;
      }
    );

    // get user connection status
    this.isAuth = this.authService.getIsAuth();
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }
}
