import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CreateUserComponent } from "./create-user/create-user.component";
import { ProfileComponent } from "./profile/profile.component";
import { SettingComponent } from "./setting/setting.component";
import { UserRoutingModule } from "./user-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxImgModule } from "ngx-img";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";

@NgModule({
  declarations: [CreateUserComponent, ProfileComponent, SettingComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxImgModule,
    NgMultiSelectDropDownModule
  ]
})
export class UserModule {}
