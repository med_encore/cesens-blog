import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { UserAuth, User } from "src/model/User.model";
import { Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { UserService } from "src/services/api/user.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { Post } from "src/model/Post.model";
import { Category } from "src/model/Category.model";
import { CategoryService } from "src/services/api/category.service";
import { PostService } from "src/services/api/post.service";
import { AlertService } from "ngx-alerts";

@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  styleUrls: ["./setting.component.css"]
})
export class SettingComponent implements OnInit, OnDestroy {
  // loading
  loading: boolean;

  // navigation
  nav = "account";
  href: string;
  mode: string;

  // current user
  isAuth: boolean;
  user: UserAuth;
  userSub: Subscription;
  // fetchedUser: User;

  // The form
  pwdForm: FormGroup;
  emailForm: FormGroup;

  // multi select users
  dropdownListUsers: User[];
  selectedItemsUsers = [];
  dropdownSettingsUsers: IDropdownSettings = {};
  selectedUser: User;

  // multi select posts
  dropdownListPosts: Post[];
  selectedItemsPosts = [];
  dropdownSettingsPosts: IDropdownSettings = {};
  selectedPost: Post;

  // multi select categories
  dropdownListCategories: Category[];
  selectedItemsCategories = [];
  dropdownSettingsCategories: IDropdownSettings = {};
  selectedCategory: Category;

  // subscription
  pwdSub: Subscription;
  emailSub: Subscription;
  deactivateSub: Subscription;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private categotyService: CategoryService,
    private postService: PostService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    // get the current user (CONNECTED)
    this.userSub = this.authService.getUser().subscribe(auth => {
      this.user = auth;
      console.log(auth);
    });

    // get mode admin
    if (this.user.administrator) {
      this.mode = "admin";
    }

    // navigation and set the form
    this.href = this.router.url;
    if (this.href === "/user/setting/account") {
      this.nav = "account";

      // const email = this.user.email;
      let email = this.user.email;
      // if this user not admin then hide email
      if (true) {
        const censorWord = (str: string) => {
          return str[0] + "*".repeat(str.length - 2) + str.slice(-1);
        };
        const arr = email.split("@");
        email = censorWord(arr[0]) + "@" + arr[1];
      }

      this.emailForm = this.formBuilder.group({
        email: [email, Validators.required]
      });
    } else if (this.href === "/user/setting/security") {
      this.nav = "security";
      this.pwdForm = this.formBuilder.group(
        {
          password: ["", [Validators.required]],
          confirmPassword: [""]
        },
        { validator: this.checkPasswords }
      );
    }

    // for admin
    if (this.mode === "admin") {
      if (this.href === "/user/setting/users") {
        this.loading = true;
        this.nav = "users";
        // fetch users (not the connected user)
        this.userService.fetchAllUsers(null, null).subscribe(
          data => {
            this.dropdownListUsers = data;
          },
          err => console.log(err),
          () => {
            this.dropdownListUsers = this.dropdownListUsers.filter(
              u => u.id !== this.user.id
            );
            this.loading = false;
          }
        );
        // setting
        this.dropdownSettingsUsers = {
          singleSelection: false,
          idField: "id",
          textField: "firstName",
          selectAllText: "Select All",
          unSelectAllText: "UnSelect All",
          itemsShowLimit: 4,
          allowSearchFilter: true
        };
      } else if (this.href === "/user/setting/categories") {
        this.loading = true;
        this.nav = "categories";
        // fetch categories
        this.categotyService.fetchAllCategories(null, null).subscribe(data => {
          this.dropdownListCategories = data;
          this.loading = false;
        });
        this.dropdownSettingsCategories = {
          singleSelection: true,
          idField: "id",
          textField: "name",
          selectAllText: "Select All",
          unSelectAllText: "UnSelect All",
          itemsShowLimit: 4,
          allowSearchFilter: true
        };
      } else if (this.href === "/user/setting/posts") {
        this.loading = true;
        this.nav = "posts";
        // fetch posts
        this.postService.fetchAllPosts(null, null).subscribe(data => {
          this.dropdownListPosts = data.posts;
          this.loading = false;
        });
        this.dropdownSettingsPosts = {
          singleSelection: true,
          idField: "id",
          textField: "title",
          selectAllText: "Select All",
          unSelectAllText: "UnSelect All",
          itemsShowLimit: 4,
          allowSearchFilter: true
        };
      }
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onChangeEmail() {
    const email = this.emailForm.get("email").value;
    if (!this.validateEmail(email) || email.indexOf("*") > -1) {
      this.alertService.danger("Your email is not valid");
      return;
    }
    this.emailSub = this.userService.changeEmail(this.user.id, email).subscribe(
      response => {
        this.alertService.success("your email has been changed");
      },
      err => this.alertService.danger(err.error.msg),
      () => {
        this.authService.logout();
      }
    );
  }

  checkPasswords(group: FormGroup) {
    // here we have the 'passwords' group
    const pass = group.get("password").value;
    const confirmPass = group.get("confirmPassword").value;
    return pass === confirmPass ? null : { notSame: true };
  }

  onChangePassword() {
    if (this.pwdForm.invalid) {
      this.alertService.danger("Password is not valid");
      return;
    }
    // todo validate pwd
    const pwd = this.pwdForm.get("password").value;
    this.emailSub = this.userService
      .changePassword(this.user.id, pwd)
      .subscribe(
        response => {
          this.alertService.success("your password has been changed");
        },
        err => console.log(err),
        () => this.authService.logout()
      );
  }

  onDeactivateAccount() {
    this.alertService.info("You can't deactivate account now");
    // this.deactivateSub = this.userService
    //   .deactivateUser(this.user.id)
    //   .subscribe(
    //     response => {
    //       // todo redirect to the users page
    //       console.log(response);
    //     },
    //     err => console.log(err),
    //     () => console.log("deactivate user notification")
    //   );
  }

  onEdit(model: string) {
    switch (model) {
      case "category":
        if (this.selectedCategory) {
          this.router.navigate(["/category/edit/", this.selectedCategory.id]);
        }
        return;
      case "user":
        if (this.selectedUser) {
          this.router.navigate(["/user/edit/", this.selectedUser.id]);
        }
        return;
      case "post":
        if (this.selectedPost) {
          this.router.navigate(["/post/edit/", this.selectedPost.id]);
        }
        return;
      default:
        return;
    }
  }

  onCategorySelect(item: Category) {
    this.selectedCategory = item;
  }

  onUserSelect(item: User) {
    this.selectedUser = item;
  }

  onPostSelect(item: Post) {
    this.selectedPost = item;
  }

  onDeactivateUser() {
    // todo deactivate user
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.emailSub) {
      this.emailSub.unsubscribe();
    }
    if (this.pwdSub) {
      this.pwdSub.unsubscribe();
    }
    if (this.deactivateSub) {
      this.deactivateSub.unsubscribe();
    }
  }
}
