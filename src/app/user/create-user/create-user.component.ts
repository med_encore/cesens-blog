import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { UserAuth, User } from "src/model/User.model";
import { Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { UserService } from "src/services/api/user.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AlertService } from "ngx-alerts";

@Component({
  selector: "app-create-user",
  templateUrl: "./create-user.component.html",
  styleUrls: ["./create-user.component.css"]
})
export class CreateUserComponent implements OnInit, OnDestroy {
  // MODE EDIT OR CREATE POST
  private id: string;
  mode: string;

  // The form
  userForm: FormGroup;
  get firstName() {
    return this.userForm.get("firstName");
  }
  get lastName() {
    return this.userForm.get("lastName");
  }
  get email() {
    return this.userForm.get("email");
  }
  get password() {
    return this.userForm.get("password");
  }
  get confirmPassword() {
    return this.userForm.get("confirmPassword");
  }
  get gender() {
    return this.userForm.get("gender");
  }
  get description() {
    return this.userForm.get("description");
  }
  get adminPrivileges() {
    return this.userForm.get("adminPrivileges");
  }
  get photo() {
    return this.userForm.get("photo");
  }

  get linkedIn() {
    return this.userForm.get("linkedIn");
  }
  get facebook() {
    return this.userForm.get("facebook");
  }

  // generated pwd
  generatedPwd: string;

  // current user
  user: UserAuth;
  userSub: Subscription;

  // fetched user
  fetchedUser: User;
  fetchedUserSub: Subscription;

  // image inupt
  optionsImg = {
    fileSize: 2048, // in Bytes (by default 2048 Bytes = 2 MB)
    minWidth: 0, // minimum width of image that can be uploaded (by default 0, signifies any width)
    maxWidth: 0, // maximum width of image that can be uploaded (by default 0, signifies any width)
    minHeight: 0, // minimum height of image that can be uploaded (by default 0, signifies any height)
    maxHeight: 0, // maximum height of image that can be uploaded (by default 0, signifies any height)
    fileType: ["image/gif", "image/jpeg", "image/png"], // mime type of files accepted
    height: 411, // height of cropper
    quality: 1, // quaity of image after compression
    crop: [
      // array of objects for mulitple image crop instances (by default null, signifies no cropping)
      {
        ratio: 1, // ratio in which image needed to be cropped (by default null, signifies ratio to be free of any restrictions)
        minWidth: 411, // minimum width of image to be exported (by default 0, signifies any width)
        maxWidth: 811, // maximum width of image to be exported (by default 0, signifies any width)
        minHeight: 411, // minimum height of image to be exported (by default 0, signifies any height)
        maxHeight: 811, // maximum height of image to be exported (by default 0, signifies any height)
        width: 411, // width of image to be exported (by default 0, signifies any width)
        height: 411 // height of image to be exported (by default 0, signifies any height)
      }
    ]
  };

  // image edit model
  ImageModalRef: BsModalRef;
  srcImg: string = null;

  // subscriptions
  routeSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    // get the current user (CONNECTED)
    this.userSub = this.authService.getUser().subscribe(auth => {
      this.user = auth;
    });

    // set the form
    this.userForm = this.formBuilder.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [
        null,
        this.mode === "admin" || this.mode === "create"
          ? Validators.required
          : null
      ],
      description: [null],
      linkedIn: [null],
      facebook: [null],
      photo: [null, Validators.required],
      gender: [null],
      adminPrivileges: [
        null,
        this.mode === "admin" || this.mode === "create"
          ? Validators.required
          : null
      ],
      password: [
        null,
        this.mode === "admin" || this.mode === "create"
          ? Validators.required
          : null
      ],
      confirmPassword: [
        null,
        this.mode === "admin" || this.mode === "create"
          ? Validators.required
          : null
      ]
    });

    // SELECT MODE (create as admin - edit - edit as admin)
    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("id")) {
        // todo check if value is number
        this.id = paramMap.get("id");
        if (this.user.administrator) {
          this.mode = "admin";
          // fetch user
          this.getUser(this.mode);
        } else {
          this.mode = "edit";
          // fetch user
          this.getUser(this.mode);
        }
      } else {
        this.mode = "create";
        this.id = null;
      }
    });
    console.log(this.mode);
  }

  openModalImage(image: TemplateRef<any>) {
    this.ImageModalRef = this.modalService.show(
      image,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  onSelectImage(event: string | object) {
    console.log(event);

    switch (typeof event) {
      case "string":
        this.srcImg = event;
        this.userForm.patchValue({ photo: [event] });
        this.userForm.get("photo").updateValueAndValidity();
        break;
      case "object":
        this.userForm.patchValue({ photo: event });
        this.userForm.get("photo").updateValueAndValidity();
        break;
      default:
    }
    this.userForm.patchValue({ photo: event });
    this.userForm.get("photo").updateValueAndValidity();
  }

  onResetImage() {
    this.srcImg = null;
  }

  generatePwd() {
    const pwd = Math.random()
      .toString(36)
      .slice(-8);
    this.userForm.patchValue({ password: pwd });
    this.userForm.patchValue({ confirmPassword: pwd });
    this.generatedPwd = pwd;
  }

  onChangedPwd() {
    this.generatedPwd = "";
  }

  private getUser(mode: string) {
    this.fetchedUserSub = this.userService.fetchUser(Number(this.id)).subscribe(
      user => {
        this.fetchedUser = user;
        this.userForm.setValue({
          firstName: this.fetchedUser.firstName,
          lastName: this.fetchedUser.lastName,
          email: mode === "admin" ? this.fetchedUser.email : null,
          linkedIn: this.fetchedUser.linkedin ? this.fetchedUser.linkedin : "",
          facebook: this.fetchedUser.facebook ? this.fetchedUser.facebook : "",
          password: "",
          confirmPassword: "",
          description: this.fetchedUser.description,
          gender: mode === "admin" ? this.fetchedUser.gender : null,
          adminPrivileges:
            mode === "admin" ? this.fetchedUser.administrator : null,
          photo: ""
        });
      },
      err => console.log(err),
      () => console.log("user fetched")
    );
  }

  onSubmit() {
    const data = {
      firstName: this.userForm.get("firstName").value,
      lastName: this.userForm.get("lastName").value,
      email: this.userForm.get("email").value,
      gender: this.userForm.get("gender").value,
      description: this.userForm.get("description").value,
      photo: this.userForm.get("photo").value,
      administrator:
        this.userForm.get("adminPrivileges").value === true ? true : false,
      password: this.userForm.get("password").value
    };
    if (this.mode === "create") {
      // create mode
      this.userService.createUser(data).subscribe(
        response => {
          // todo redirect to the users page
          console.log(response);
        },
        err => console.log(err),
        () => this.router.navigate(["/user/profile"])
      );
    } else {
      this.userService.editUser(Number(this.id), data).subscribe(
        response => {
          // todo redirect to the users page
          console.log(response);
        },
        err => console.log(err),
        () => this.router.navigate(["/user/profile"])
      );
    }
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.fetchedUserSub) {
      this.fetchedUserSub.unsubscribe();
    }
  }
}
