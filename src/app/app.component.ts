import { AuthService } from "./auth/auth.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  auth: boolean;
  authSub: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.authService.autoAuthUser();
    this.authSub = this.authService.getAuthStatusListener().subscribe(auth => {
      this.auth = auth;
    });
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }
}
