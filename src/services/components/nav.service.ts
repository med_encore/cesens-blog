import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class NavService {
  visible: boolean;
  private loadingState = new BehaviorSubject(false);
  private visibleState = new BehaviorSubject(true);

  getVisible() {
    return this.visibleState.asObservable();
  }

  getLoading() {
    return this.loadingState.asObservable();
  }

  constructor() {
    this.visible = true;
    this.visibleState.next(true);
    this.loadingState.next(false);
  }

  isLoading(bol: boolean) {
    this.loadingState.next(bol);
  }

  hide() {
    this.visible = false;
    this.visibleState.next(false);
  }

  show() {
    this.visible = true;
    this.visibleState.next(true);
  }
}
