import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler
} from "@angular/common/http";
import { Injectable } from "@angular/core";

import { AuthService } from "../../app/auth/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  authToken: string;
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.authService.getToken().subscribe(token => {
      this.authToken = token;
    });
    const authRequest = req.clone({
      headers: req.headers.set("Authorization", "Bearer " + this.authToken)
    });
    return next.handle(authRequest);
  }
}
