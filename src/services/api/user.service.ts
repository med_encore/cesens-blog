import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { User } from "src/model/User.model";
import { Post } from "src/model/Post.model";

const URL = environment.baseUrl + "users";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private http: HttpClient, private router: Router) {}

  fetchAllUsers(page: number, limit: number) {
    if (page !== null && limit !== null) {
      return this.http.get<User[]>(`${URL}?page=${page}&&limit=${limit}`);
    }
    return this.http.get<User[]>(URL);
  }

  fetchAllPostsByUser(id: number, page: number, limit: number) {
    if (page !== null && limit !== null) {
      return this.http.get<Post[]>(
        `${URL}/posts/${id}?page=${page}&&limit=${limit}`
      );
    }
    return this.http.get<Post[]>(`${URL}/posts/${id}`);
  }

  fetchUser(id: number) {
    return this.http.get<User>(`${URL}/${id}`);
  }

  createUser(data: User) {
    return this.http.post<User>(`${URL}`, data);
  }

  editUser(id: number, data: User) {
    return this.http.put<User>(`${URL}/edit/${id}`, data);
  }

  changePassword(id: number, password: string) {
    return this.http.post<User>(`${URL}/password`, { id, password });
  }

  changeEmail(id: number, email: string) {
    return this.http.post<User>(`${URL}/email`, { id, email });
  }

  deactivateUser(id: number) {
    return this.http.get<User>(`${URL}/edit/deactivate/${id}`);
  }

  deleteUser(id: number) {
    return this.http.delete<boolean>(`${URL}/delete/${id}`);
  }
}
