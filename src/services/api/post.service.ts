import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Post } from "src/model/Post.model";
import { User } from "src/model/User.model";
import { Category } from "src/model/Category.model";

const URL = environment.baseUrl + "posts";

@Injectable({
  providedIn: "root"
})
export class PostService {
  constructor(private http: HttpClient, private router: Router) {}

  fetchAllPosts(page: number, limit: number) {
    if (page !== null && limit !== null) {
      return this.http.get<{ posts: Post[]; length: number }>(
        `${URL}?page=${page}&&limit=${limit}`
      );
    }
    return this.http.get<{ posts: Post[]; length: number }>(URL);
  }

  fetchLastnPosts(n: number) {
    return this.http.get<Post[]>(`${URL}?number=${n}`);
  }

  fetchPost(id: number) {
    return this.http.get<{
      post: Post;
      collaborators: User[];
      categories: Category[];
    }>(`${URL}/${id}`);
  }

  createPost(data: Post) {
    return this.http.post<{
      post: Post;
      categories: Category[];
      collaborators: User[];
    }>(`${URL}`, data);
  }

  editPost(id: number, data: Post) {
    return this.http.put<Post>(`${URL}/edit/${id}`, data);
  }

  deletePost(id: number) {
    return this.http.delete<boolean>(`${URL}/delete/${id}`);
  }
}
