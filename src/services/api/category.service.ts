import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Category } from "../../model/Category.model";
import { Post } from "src/model/Post.model";
const URL = environment.baseUrl + "categories";

@Injectable({
  providedIn: "root"
})
export class CategoryService {
  constructor(private http: HttpClient, private router: Router) {}

  fetchAllCategories(page: number, limit: number) {
    if (page !== null && limit !== null) {
      return this.http.get<Category[]>(`${URL}?page=${page}&limit=${limit}`);
    }
    return this.http.get<Category[]>(URL);
  }

  fetchAllPostsByCategory(id: number, page: number, limit: number) {
    if (page !== null && limit !== null) {
      return this.http.get<{ posts: Post[]; length: number }>(
        `${URL}/posts/${id}?page=${page}&limit=${limit}`
      );
    }
    return this.http.get<{ posts: Post[]; length: number }>(
      `${URL}/posts/${id}`
    );
  }

  fetchCategory(id: number) {
    return this.http.get<Category>(`${URL}/${id}`);
  }

  createCategory(data: Category) {
    return this.http.post<Category>(`${URL}`, data);
  }

  editCategory(id: number, data: Category) {
    return this.http.put<Category>(`${URL}/edit/${id}`, data);
  }

  deleteCategory(id: number) {
    return this.http.delete<boolean>(`${URL}/delete/${id}`);
  }
}
