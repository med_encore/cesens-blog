import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class BaseService {
  constructor() {}

  stripHtml = (html: string) => {
    const regex = /(<([^>]+)>)/gi;
    return html.replace(regex, "");
    // tslint:disable-next-line: semicolon
  };
}
